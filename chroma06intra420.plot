set terminal epslatex
set output "chroma06intra420.tex"
set xlabel '$Q$'
set ylabel '$\sigma$'
set key bottom right

load 'vstats-420-speed06.txt'

#plot \
# $yintraac420 using (($2)):(sqrt($4)) title "Y' Intra AC 4:2:0" with lines linetype 4 linecolor rgb "green", \
# $cbintraac420 using (($2)):(sqrt($4)) title 'Cb Intra AC 4:2:0' with lines linetype 1 linecolor rgb "#6F6FFF", \
# $crinterac420 using (($2)):(sqrt($4)) title 'Cr Intra AC 4:2:0' with lines linetype 1 linecolor rgb "magenta"

plot \
 $yintradc420 using (($2)):(sqrt($4)) title "Y' Intra DC 4:2:0" with lines linetype 1 linecolor rgb "#007F00", \
 $yintraac420 using (($2)):(sqrt($4)) title "Y' Intra AC 4:2:0" with lines linetype 4 linecolor rgb "green", \
 $cbintradc420 using (($2)):(sqrt($4)) title 'Cb Intra DC 4:2:0' with lines linetype 1 linecolor rgb "blue", \
 $cbintraac420 using (($2)):(sqrt($4)) title 'Cb Intra AC 4:2:0' with lines linetype 1 linecolor rgb "#6F6FFF", \
 $crintradc420 using (($2)):(sqrt($4)) title 'Cr Intra DC 4:2:0' with lines linetype 4 linecolor rgb "red", \
 $crinterac420 using (($2)):(sqrt($4)) title 'Cr Intra AC 4:2:0' with lines linetype 1 linecolor rgb "magenta"
