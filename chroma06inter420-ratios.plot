set terminal epslatex
set output "chroma06inter420-ratios.tex"
set xlabel "$\\log Q_{Y'}$"
set ylabel 'Relative variance'
set key center right

load 'vstats-420-speed06.txt'
load 'vstats-422-speed06.txt'
load 'vstats-444-speed06.txt'

plot \
 $inter420ratios using (log($1)):(($4)) title "Y' Inter AC 4:2:0" with lines linetype 3 lw 3 linecolor rgb "green", \
 $inter422ratios using (log($1)):(($4)) title "Y' Inter AC 4:2:2" with lines linetype 3 lw 2 linecolor rgb "green", \
 $inter444ratios using (log($1)):(($4)) title "Y' Inter AC 4:4:4" with lines linetype 3 lw 1 linecolor rgb "green", \
 $inter420ratios using (log($1)):(($5)) title 'Cb Inter AC 4:2:0' with lines linetype 6 lw 3 linecolor rgb "#6F6FFF", \
 $inter422ratios using (log($1)):(($5)) title 'Cb Inter AC 4:2:2' with lines linetype 6 lw 2 linecolor rgb "#6F6FFF", \
 $inter444ratios using (log($1)):(($5)) title 'Cb Inter AC 4:4:4' with lines linetype 6 lw 1 linecolor rgb "#6F6FFF", \
 $inter420ratios using (log($1)):(($6)) title 'Cr Inter AC 4:2:0' with lines linetype 2 lw 3 linecolor rgb "magenta", \
 $inter422ratios using (log($1)):(($6)) title 'Cr Inter AC 4:2:2' with lines linetype 2 lw 2 linecolor rgb "magenta", \
 $inter444ratios using (log($1)):(($6)) title 'Cr Inter AC 4:4:4' with lines linetype 2 lw 1 linecolor rgb "magenta"

#plot [0:9][] \
# $yinterdc420 using (log($2)):(log(($4))) title "$Y'$ Inter DC 4:2:0" with lines linetype 4 linecolor rgb "#007F00" lw 3, \
# $yinterdc422 using (log($2)):(log(($4))) title "$Y'$ Inter DC 4:2:2" with lines linetype 4 linecolor rgb "#007F00" lw 2, \
# $yinterdc444 using (log($2)):(log(($4))) title "$Y'$ Inter DC 4:4:4" with lines linetype 4 linecolor rgb "#007F00" lw 1, \
# $yinterac420 using (log($2)):(log(($4))) title "$Y'$ Inter AC 4:2:0" with lines linetype 3 linecolor rgb "green" lw 3, \
# $yinterac422 using (log($2)):(log(($4))) title "$Y'$ Inter AC 4:2:2" with lines linetype 3 linecolor rgb "green" lw 2, \
# $yinterac444 using (log($2)):(log(($4))) title "$Y'$ Inter AC 4:4:4" with lines linetype 3 linecolor rgb "green" lw 1, \
# $cbinterdc420 using (log($2)):(log(($4))) title '$C_b$ Inter DC 4:2:0' with lines linetype 5 linecolor rgb "blue" lw 3, \
# $cbinterdc422 using (log($2)):(log(($4))) title '$C_b$ Inter DC 4:2:2' with lines linetype 5 linecolor rgb "blue" lw 2, \
# $cbinterdc444 using (log($2)):(log(($4))) title '$C_b$ Inter DC 4:4:4' with lines linetype 5 linecolor rgb "blue" lw 1, \
# $cbinterac420 using (log($2)):(log(($4))) title '$C_b$ Inter AC 4:2:0' with lines linetype 6 linecolor rgb "#6F6FFF" lw 3, \
# $cbinterac422 using (log($2)):(log(($4))) title '$C_b$ Inter AC 4:2:2' with lines linetype 6 linecolor rgb "#6F6FFF" lw 2, \
# $cbinterac444 using (log($2)):(log(($4))) title '$C_b$ Inter AC 4:4:4' with lines linetype 6 linecolor rgb "#6F6FFF" lw 1, \
# $crinterdc420 using (log($2)):(log(($4))) title '$C_r$ Inter DC 4:2:0' with lines linetype 2 linecolor rgb "red" lw 3, \
# $crinterdc422 using (log($2)):(log(($4))) title '$C_r$ Inter DC 4:2:2' with lines linetype 2 linecolor rgb "red" lw 2, \
# $crinterdc444 using (log($2)):(log(($4))) title '$C_r$ Inter DC 4:4:4' with lines linetype 2 linecolor rgb "red" lw 1, \
# $crinterac420 using (log($2)):(log(($4))) title '$C_r$ Inter AC 4:2:0' with lines linetype 1 linecolor rgb "magenta" lw 3, \
# $crinterac422 using (log($2)):(log(($4))) title '$C_r$ Inter AC 4:2:2' with lines linetype 1 linecolor rgb "magenta" lw 2, \
# $crinterac444 using (log($2)):(log(($4))) title '$C_r$ Inter AC 4:4:4' with lines linetype 1 linecolor rgb "magenta" lw 1
