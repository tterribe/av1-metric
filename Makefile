# Makefile to generate the document from its sources.
# Requires
#  transfig (for XFig figures),
#  gnuplot (for generated plots),
#  epstopdf (for EPS figures in PDF and gnuplot output in PDF),
#  imagemagick (for PNG or JPEG figures in PostScript),
#  xpdf (for PDF figures in PostScript), and
#  pdflatex (for PDF output).

DOCNAME = av1-metric

PAPER_SRCS = ${DOCNAME}.tex ${DOCNAME}.bib

EPS_SRCS =

PDF_SRCS =

PNG_SRCS =

JPEG_SRCS =

FIG_SRCS =

FIG_TEX_SRCS =

PLOT_SRCS = \
 logq.plot \
 linearq.plot \
 qratio.plot \
 qis.plot \
 apilambda.plot \
 apilambdazoom.plot \
 chroma06inter420.plot \
 chroma06inter420-ratios.plot \
 chroma06intra420.plot

FIG_PDF_SRCS = $(PDF_SRCS) $(PNG_SRCS) $(JPEG_SRCS)

FIG_PDF_OBJS = $(EPS_SRCS:.eps=.pdftag) \
 $(FIG_SRCS:.fig=.pdf) \
 $(FIG_TEX_SRCS:.fig=.pdftex_t) \
 $(PLOT_SRCS:.plot=.pdftex_t)

FIG_PDF_AUXS = $(EPS_SRCS:.eps=.pdf) \
 $(FIG_TEX_SRCS:.fig=.pdftex)  \
 $(PLOT_SRCS:.plot=.pdftex)

FIG_EPS_SRCS = $(EPS_SRCS)

FIG_EPS_OBJS = $(PDF_SRCS:.pdf=.epstag) \
 $(PNG_SRCS:.png=.eps) \
 $(patsubst %.jpeg,%.eps,$(JPEG_SRCS:.jpg=.eps)) \
 $(FIG_SRCS:.fig=.eps) \
 $(FIG_TEX_SRCS:.fig=.pstex_t) \
 $(PLOT_SRCS:.plot=.pstex_t)

FIG_EPS_AUXS = $(PDF_SRCS:.pdf=.eps) \
 $(FIG_TEX_SRCS:.fig=.pstex) \
 $(PLOT_SRCS:.plot=.pstex)

EXTRA_FIGS = \

#oofigs: pdf
#	pdftops -f 1 -l 1 -eps $(DOCNAME).pdf dct4.eps
#	sed -e 's/^%%BoundingBox:.*/%%BoundingBox: 225 572 387 666/' -i dct4.eps
#	pdftops -f 2 -l 2 -eps $(DOCNAME).pdf dct8.eps
#	sed -e 's/^%%BoundingBox:.*/%%BoundingBox: 135 459 536 667/' -i dct8.eps
#	pdftops -f 3 -l 3 -eps $(DOCNAME).pdf dct16.eps
#	sed -e 's/^%%BoundingBox:.*/%%BoundingBox: 135 449 530 667/' -i dct16.eps

pdf: ${DOCNAME}.pdf

ps: ${DOCNAME}.ps

all: ${DOCNAME}.pdf ${DOCNAME}.ps

${DOCNAME}.pdf : $(PAPER_SRCS) $(FIG_PDF_OBJS) $(FIG_PDF_SRCS) ${DOCNAME}.bbl
	pdflatex ${DOCNAME}
	pdflatex ${DOCNAME}

${DOCNAME}.bbl : $(PAPER_SRCS) $(FIG_PDF_OBJS)
	if [ ${DOCNAME}.tex -nt ${DOCNAME}.aux ] ; then \
          pdflatex ${DOCNAME} ; \
        fi
	bibtex ${DOCNAME}

${DOCNAME}.ps : $(PAPER_SRCS) $(FIG_EPS_OBJS) $(FIG_EPS_SRCS)
	latex ${DOCNAME}
	latex ${DOCNAME}
	dvips ${DOCNAME} -o $@

# rules for generating figures

figures : $(FIG_PDF_OBJS) $(FIG_EPS_OBJS)

%.pdftag : %.eps
	epstopdf $< -o $(<:.eps=.pdf)
	touch $@

%.epstag : %.pdf
	pdftops -eps $< $(<:.pdf=.eps)
	touch $@

%.png : %.eps
	convert $< $@

%.jpg : %.eps
	convert $< $@

%.jpeg : %.eps
	convert $< $@

#%.tex : %.fig
#	fig2dev -L latex $< $@

%.eps : %.fig
	fig2dev -L eps $< $@

%.pdf : %.fig
	fig2dev -L pdf -p 0 $< $@

%.pdftex_t : %.fig
	fig2dev -L pdftex -p 0 $< $(<:.fig=.pdftex)
	fig2dev -L pdftex_t -p $(<:.fig=.pdftex) $< $@

%.pstex_t : %.fig
	fig2dev -L pstex -p 0 $< $(<:.fig=.pstex)
	fig2dev -L pstex_t -p $(<:.fig=.pstex) $< $@

%.pdftex_t : %.plot
	gnuplot $<
	epstopdf $(<:.plot=.eps) -o $(<:.plot=.pdftex)
	-rm $(<:.plot=.eps)
	mv $(<:.plot=.tex) $@

%.pstex_t : %.plot
	gnuplot $<
	mv $(<:.plot=.eps) $(<:.plot=.pstex)
	mv $(<:.plot=.tex) $@

# Make everything depend on changes in the Makefile
${DOCNAME}.pdf ${DOCNAME}.bbl $(FIG_PDF_OBJS) $(FIG_EPS_OBJS): Makefile

# clean targets
clean:
	-rm ${DOCNAME}.pdf
	-rm $(FIG_PDF_OBJS)
	-rm $(FIG_PDF_AUXS)
	-rm ${DOCNAME}.ps
	-rm ${DOCNAME}.dvi
	-rm $(FIG_EPS_OBJS)
	-rm $(FIG_EPS_AUXS)
	-rm ${DOCNAME}.aux
	-rm ${DOCNAME}.log
	-rm ${DOCNAME}.lof
	-rm ${DOCNAME}.lot
	-rm ${DOCNAME}.out
	-rm ${DOCNAME}.bbl
	-rm ${DOCNAME}.blg
	-rm ${DOCNAME}.toc

distclean: clean

maintainer-clean: distclean

maintainerclean: maintainer-clean

.PHONY: all pdf ps figures clean distclean maintainer-clean
